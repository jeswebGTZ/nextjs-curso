//aqui va la aparte de la data del servidor 
'use server';

//biblioteca de validacion
import { z } from 'zod';
//biblio sql insercion 
import { sql } from '@vercel/postgres';
//lib para borrar cahce y activar nueva solicitud  
import { revalidatePath } from 'next/cache';
//redirigir al user 
import { redirect } from 'next/navigation';
import { Customer } from './definitions';


const FormSchema = z.object({
    id: z.string(),
    customerId: z.string(),
    amount: z.coerce.number(),
    status: z.enum(['pending', 'paid']),
    date: z.string(),
});

/* crear registro */
const CreateInvoice = FormSchema.omit({ id: true, date: true });
export async function createInvoice(formData: FormData) {
    const { customerId, amount, status } = CreateInvoice.parse({
        customerId: formData.get('customerId'),
        amount: formData.get('amount'),
        status: formData.get('status'),
    });
    //cantidad a centavos 
    const amountInCents = amount * 100;
    //creacion de fecha  "aa-mm-dd"
    const date = new Date().toISOString().split('T')[0];
    //inserccion a db 

    try {
        await sql`
        INSERT INTO invoices (customer_id, amount, status, date)
        VALUES (${customerId},${amountInCents},${status},${date})
        `;
    } catch (error) {
        return {
            message: 'Database Error: Failed to Create Invoice.',
        }
    }

    revalidatePath('/dashboard/invoices');
    redirect('/dashboard/invoices');


}

/*editar regirsto  */
const UpdateInvoice = FormSchema.omit({ id: true, date: true });
//funcion de actualizar 

export async function updateInvoice(id: string, formData: FormData) {
    const { customerId, amount, status } = UpdateInvoice.parse({
        customerId: formData.get('customerId'),
        amount: formData.get('amount'),
        status: formData.get('status'),
    });

    const amountInCents = amount * 100;

    try {
        await sql`
    UPDATE invoices
    SET customer_id = ${customerId}, amount = ${amountInCents}, status = ${status}
    WHERE id = ${id}
   `;
    } catch (error) {
        return { message: 'Database Error: Failed to Update Invoice.' };
    }

    revalidatePath('/dashboard/invoices');
    redirect('/dashboard/invoices');

}

/* eliminar registro  */
export async function deleteInvoice(id: string) {
    throw new Error('Failed to Delete Invoice');
    try {
        await sql`DELETE FROM invoices WHERE id = ${id}`;
        revalidatePath('/dashboard/invoices');
        return { message: 'Deleted Invoice.' };
    } catch (error) {
        return { message: 'Database Error: Failed to Delete Invoice.' };  
    }
    
}